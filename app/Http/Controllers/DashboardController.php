<?php

namespace App\Http\Controllers;

use App\Services\NumberService;
use App\Utils\Values;
use Carbon\Carbon;

class DashboardController extends Controller
{
    public function accueil()
    {
        return view('dashboard.accueil');
    }

    public function tableau()
    {
        $t = substr(Carbon::now(), 11, 5);
        $d = substr(Carbon::today(), 8, 2);
        $m = substr(Carbon::today(), 5, 2);

        $months = [
            '01' => 'Janvier',
            '02' => 'Février',
            '03' => 'Mars',
            '04' => 'Avril',
            '05' => 'Mai',
            '06' => 'Juin',
            '07' => 'Juillet',
            '08' => 'Août',
            '09' => 'Septembre',
            '10' => 'Octobre',
            '11' => 'Novembre',
            '12' => 'Décembre',
        ];
        $m = $months[$m];

        $y = substr(Carbon::today(), 0, 4);

        $pAP = new Values();
        $p = $pAP->getAllPrograms();
        $xUs = $pAP->soldModerlab() / $p;
        $xOther = $pAP->globalSold() / $p;

        $s = new NumberService();
        $x = $s->arrondir($xUs, $xOther);

        $c = $pAP->index();

        return view('dashboard.show', compact('t', 'd', 'm', 'y', 'p', 'x', 'c'));
    }
}
