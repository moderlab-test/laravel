<?php

namespace App\Utils;

class Values
{
    const BRAND = 'MODERLAB';

    public function index(): array
    {
        return [
            'France' => 151,
            'Suisse' => 2,
            'Belgique' => 1,
        ];
    }

    public function getAllPrograms(): int
    {
        return array_sum($this->index());
    }

    public function globalSold(): int
    {
        return 11;
    }

    public function soldModerlab(): int
    {
        return 40;
    }

    public static function nbUsers(): int
    {
        return 5147;
    }
}
