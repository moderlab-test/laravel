<?php

namespace App\Services;

class NumberService
{
    public function arrondir($x, $y): float
    {
        return round($x / $y, 2);
    }
}
