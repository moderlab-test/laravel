<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Test Laravel</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@200;600&display=swap" rel="stylesheet">

        <!-- Styles -->
        <style>
            html, body {
                background-color: #fff;
                color: #636b6f;
                font-family: 'Nunito', sans-serif;
                font-weight: 200;
                height: 100vh;
                margin: 0;
            }

            .full-height {
                height: 100vh;
            }

            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }

            .position-ref {
                position: relative;
            }

            .content {
                text-align: center;
            }

            .title {
                font-size: 84px;
            }

            .m-b-md {
                margin-bottom: 30px;
            }
        </style>
    </head>
    <body>
        <div class="flex-center position-ref full-height">
            <div class="content">
                <div class="title m-b-md">
                    BackOffice {{ \App\Utils\Values::BRAND }}
                </div>

                <div>
                    <p>
                        Voici quelques informations :
                    </p>
                </div>

                <div class="flex-center">
                    <table>
                        <tr>
                            <th>Heure</th>
                            <th>Jour</th>
                            <th>Mois</th>
                            <th>Année</th>
                        </tr>

                        <tr>
                            <td>{{ $t }}</td>
                            <td>{{ $d }}</td>
                            <td>{{ $m }}</td>
                            <td>{{ $y }}</td>
                        </tr>
                    </table>
                </div>

                <div class="flex-center">
                    <p>
                        Nous avons actuellement {{ $p }} programmes neufs (VEFA) en vente.
                        <br>
                        {{ \App\Utils\Values::nbUsers() }} clients nous font confiance aujourd'hui.
                        <br>
                        Utiliser nos outils permet de vendre {{ $x }} fois plus et plus rapidement.
                    </p>
                </div>

                <div class="flex-center">
                    <table>
                        <tr>
                            <th>Pays</th>
                            <th>Nombre de programmes</th>
                        </tr>

                        @foreach($c as $country => $number)
                            <tr>
                                <td>{{ $country }}</td>
                                <td>{{ $number }}</td>
                            </tr>
                        @endforeach
                    </table>
                </div>
            </div>
        </div>
    </body>
</html>
