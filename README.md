# Test Laravel Moderlab

## Objectifs

- Factoriser le code (bonnes pratiques Laravel / PHP / SOLID)


- Ajouter quelques tests unitaires, par exemple:
  - pour s'assurer que la fonction `arrondir()` retourne bien un float quoiqu'il arrive
  - pour s'assurer que la fonction `getAllPrograms()` retourne bien un int quoiqu'il arrive
  - pour sécuriser le code factorisé

## Contexte

Il ne s'agit pas d'un test chronométré.

Cependant, le token GitLab est valable 7 jours seulement.

## Installation

```shell
composer i
php artisan serve
```
